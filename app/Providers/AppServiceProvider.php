<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Channel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('*',function($view){
            //$view->with('channels', \App\Channel::all());
            $channels = \Cache::rememberForEver('channels', function(){
                return Channel::all();
            });

            $view->with('channels',$channels);
        });

        \Validator::extend('spamfree', 'App\Rules\SpamFree@passes');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
