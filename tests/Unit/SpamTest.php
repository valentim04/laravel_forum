<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Inspections\Spam;

class SpamTest extends TestCase
{
	/** @test */
	public function it_chechs_for_invalid_keywords()
	{
		$spam = new Spam();
		$this->assertFalse($spam->detect('Innocent reply here'));
		$this->expectException('Exception');
		$spam->detect('yahoo customer support');
	}

	/** @test */
	public function it_checks_for_key_being_held_down($value='')
	{
		$spam = new Spam();
		$this->expectException('Exception');
		$spam->detect('Hello World aaaaaaaaaaaa');
	}
}
